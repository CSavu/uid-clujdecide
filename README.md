# UID - ClujDecide

**ClujDecide** is a project aimed at enhancing civic engagement and decision-making in the Cluj-Napoca community. The project focuses on providing a user-friendly interface for citizens to participate in the decision-making process for various aspects related to the city's development.
